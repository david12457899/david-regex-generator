import sys
from PyQt5.QtWidgets import QApplication, QLabel, QPushButton, QVBoxLayout, QWidget, QFileDialog, QGridLayout, QLineEdit
from PyQt5.QtGui import QPixmap
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QCursor

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.setWindowTitle("Regex Generator")
        self.setGeometry(0, 0, 696, 840)
        self.setStyleSheet("background: black;")
        self.setFixedSize(self.size())

        # widgets
        self.nextX = 0
        self.nextY = 0
        
        self.initUI()

        self.show()

    def initUI(self):
        # Create logo
        pixmap = QPixmap("Images\\RegEx.png")
        self.logo = QLabel(self)
        pixmap = pixmap.scaled(200, 200, QtCore.Qt.KeepAspectRatio)
        self.logo.setPixmap(pixmap)

        self.logo.setStyleSheet("margin-top: 50px;")
        self.logo.setAlignment(QtCore.Qt.AlignCenter)
        self.logo.move(248, 0)

        # Creating buttons
        # Generate regex button
        generate_button = self.create_button("GENERATE", 198, 630)
        generate_button.clicked.connect(self.generate_regex)
        # Save regex button
        save_button = self.create_button("SAVE", 198, 735)
        save_button.clicked.connect(self.save_regex)

        # Creating text boxes
        self.create_textbox(35, 270)
        self.create_textbox(366, 270)

    def generate_regex(self):
        pass

    def save_regex(self):
        pass

    def create_textbox(self, x, y):
        regex_textbox = QLineEdit(self)
        regex_textbox.setGeometry(x, y, 296, 330)
        regex_textbox.set
        pass
    
    def create_button(self, text, x, y, width=300, height=70):
        button = QPushButton(text, self)
        button.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        button.setStyleSheet(
            "*{border: 4px solid '#114662';" +
            "border-radius: 15px;" +
            "font-size: 35px;" +
            "color:white;}" +
            "*:hover{background: '#114662';}"
        )
        button.setGeometry(x, y, width, height)
        return button


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec())




